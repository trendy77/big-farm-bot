/******************/
/* OBJECT CLASSES */
/******************/

/* Class Element : Base class for other classes */

var Element = function(img_path, img_mask_path) {
    this.img_path = img_path;
    this.img   = new Image(img_path);
    this.img_mask = (typeof img_mask_path == "undefined") ? undefined : new Image(img_mask_path);

};

Element.prototype.find = function(findMatchFunction, findMaskedMatchFunction) {

    // Looks for the element and returns it if valid. Undefined otherwise.
    if (this.img_mask) {
        var match = findMaskedMatchFunction(Browser.takeScreenshot().grayed(), this.img, this.img_mask, 0.98);
    } else {
        var match = findMatchFunction(Browser.takeScreenshot(), this.img, 0.98);
    }

    return match;
};

Element.prototype.findOne = function() {
    var match = this.find(Vision.findMatch, Vision.findMaskedMatch);
    return (match === undefined) ? [] : [match];
};

Element.prototype.findAll = function() {
    return this.find(Vision.findMatches, Vision.findMaskedMatches);
};

Element.prototype.execute = function(matches, action, offset_x, offset_y, debug_msg, wait_time) {

    wait_time = (typeof wait_time == "undefined") ? 350 : wait_time;

    // If the element is not visible, returns False without doing anything
    if (matches.length == 0) {
        Helper.debug(debug_msg);
        return false;
    }

    // Click the found matches with the given offset
    for (var i = matches.length - 1; i >= 0; i--) {
        var match_center = matches[i].getRect().getCenter();
        action(new Point(match_center.getX() + offset_x, match_center.getY() + offset_y));
        Helper.msleep(wait_time); // Wair for animations to end after executing the action
    }

    return true;
};

Element.prototype.executeActionOnOne = function(offset_x, offset_y, action, debug_msg) {

    // Default parameters
    offset_x = (typeof offset_x == "undefined") ? 0 : offset_x;
    offset_y = (typeof offset_y == "undefined") ? 0 : offset_y;

    var matches = this.findOne();

    this.execute(matches, action, offset_x, offset_y, debug_msg);
};

Element.prototype.executeActionOnAll = function(offset_x, offset_y, action, debug_msg) {

    // Default parameters
    offset_x = (typeof offset_x == "undefined") ? 0 : offset_x;
    offset_y = (typeof offset_y == "undefined") ? 0 : offset_y;

    var matches = this.findAll();

    this.execute(matches, action, offset_x, offset_y, debug_msg);
};


// Returns True if the element was found and clicked. False otherwise.
Element.prototype.click = function(offset_x, offset_y) {

    const debug_msg = "Couldn't click element: " + this.img_path + ". Not found."
    this.executeActionOnOne(offset_x, offset_y, Browser.leftClick, debug_msg);
};

Element.prototype.hover = function(offset_x, offset_y) {
    
    const debug_msg = "Couldn't hover element: " + this.img_path + ". Not found."
    this.executeActionOnOne(offset_x, offset_y, Browser.moveMouseTo, debug_msg);
};

Element.prototype.clickAll = function(offset_x, offset_y) {

    const debug_msg = "Couldn't click elements: " + this.img_path + ". Not found."
    this.executeActionOnAll(offset_x, offset_y, Browser.leftClick, debug_msg);
};


/* - End of Element Class - */


/*************/
/* RESOURCES */
/*************/

// Buttons
var close_button = new Element("img/buttons/close_button.png");
var config_dropdown_button = new Element("img/buttons/config_dropdown_button.png");
var config_grid_button = new Element("img/buttons/config_grid_button.png");

// Citizen Alerts
var citizen_sack_alert = new Element("img/alerts/citizen_alerts/sack_big_alert_2.png", "img/alerts/citizen_alerts/sack_big_alert_mask_2.png");

// Building Alerts
var corn_ready_alert = new Element("img/alerts/building_alerts/corn_ready_alert.png");
var chicken_food_ready_alert = new Element("img/alerts/building_alerts/chicken_food_ready_alert.png");
var egg_ready_alert = new Element("img/alerts/building_alerts/egg_ready_alert.png");
var fertilizer_ready_alert = new Element("img/alerts/building_alerts/fertilizer_ready_alert.png");
var blue_idle_alert = new Element("img/alerts/building_alerts/blue_idle_alert.png");

// Building Buttons
var plant_button = new Element("img/buttons/plant_button.png");
var chicken_food_button = new Element("img/buttons/chicken_food_button.png");
var produce_button = new Element("img/buttons/produce_button.png");
var fertilizer_button = new Element("img/buttons/fertilizer_button.png");

// Food and Seed Sacks variables
var offset_short_sack_button = new Point(60, 72);
var offset_large_sack_button = new Point(228, 55);
var corn_seed_sack = new Element("img/buildings/corn_seed_sack.png");
var chicken_food_sack = new Element("img/buildings/chicken_food_sack.png");
var fertilizer_sack = new Element("img/buildings/fertilizer_sack.png");

/******************/
/* GAME FUNCTIONS */
/******************/

function closeDialogWindows() {

    while (close_button.click() == true) { 
    }
};

function collectCitizenGift() {
    citizen_sack_alert.hover();
};


var collect_buildings_timer = undefined;
function collectBuildingProducts() {

    current_time = new Date();

    if (collect_buildings_timer === undefined || (current_time - collect_buildings_timer)/60000 >= 1) {

        Helper.debug("Starting collection of goods from buildings");

        corn_ready_alert.clickAll();
        chicken_food_ready_alert.clickAll();
        egg_ready_alert.clickAll();
        fertilizer_ready_alert.clickAll();

        collect_buildings_timer = current_time;
    }else {

        Helper.debug("Skipping collection of goods in buildings");
    }
    
};

function plantCornFields() {

    function plantCornAction(field_point) {
        Browser.leftClick(field_point);
        Helper.msleep(250);
        plant_button.click();
        corn_seed_sack.click(offset_large_sack_button.getX(), offset_large_sack_button.getY());
    }

    blue_idle_alert.executeActionOnAll(0, 0, plantCornAction, "No idle buldings - Skipping planting corn");

};

function produceChickenFood() {

    function produceChickenFoodAction(windmill_point) {
        Browser.leftClick(windmill_point);
        Helper.msleep(250);
        produce_button.click();
        chicken_food_sack.click(offset_short_sack_button.getX(), offset_short_sack_button.getY());
        closeDialogWindows();
    }

    blue_idle_alert.executeActionOnAll(0, 0, produceChickenFoodAction, "No idle buldings - Skipping chicken food production");

};

function feedChickens() {

    function feedChickensAction(henhouse_point) {
        Browser.leftClick(henhouse_point);
        Helper.msleep(250);
        chicken_food_button.click();
    }

    blue_idle_alert.executeActionOnAll(0, 0, feedChickensAction, "No idle buldings - Skipping feeding chickens");
};

function produceFertilizer() {

    function produceFertilizerAction(silo_point) {
        Browser.leftClick(silo_point);
        Helper.msleep(250);
        produce_button.click();
        fertilizer_sack.click(offset_short_sack_button.getX(), offset_short_sack_button.getY());
        closeDialogWindows();
    }

    blue_idle_alert.executeActionOnAll(0, 0, produceFertilizerAction, "No idle buldings - Skipping fertilizer production");
}

function produceApples() {

    function produceApplesAction(field_point) {
        Browser.leftClick(field_point);
        Helper.msleep(250);
        fertilizer_button.click();
    }

    blue_idle_alert.executeActionOnAll(0, 0, produceApplesAction, "No idle buldings - Skipping apples production");
}

var produce_buildings_timer = undefined;
function produceBuildingProducts() {
    current_time = new Date();

    if (produce_buildings_timer === undefined || (current_time - produce_buildings_timer )/60000 >= 1) {

        Helper.debug("Starting production of goods in buildings");

        plantCornFields();
        produceChickenFood();
        feedChickens();
        produceFertilizer();
        produceApples();

        produce_buildings_timer = current_time;
    } else {

        Helper.debug("Skipping production of goods in buildings");
    }
}

function initialSettings() {

    closeDialogWindows(); // Close all annoying windows at start
};

/* MAIN FUNCTION */

function main() {

    Helper.log("Starting bot...");
    Helper.sleep(5);
    initialSettings();

    while (true) {
        closeDialogWindows();
        collectCitizenGift();
        collectBuildingProducts();
        produceBuildingProducts();
        Helper.sleep(2);
    }

};

main();